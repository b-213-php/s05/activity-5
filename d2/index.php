<!DOCTYPE html>

<html>

    <head>

        <title>S05: Client-Server Communication (Basic To-Do)</title>

    </head>

    <body>
        <?php session_start(); ?>
        <!-- create new session for us to be able to send and receive data across different clirnts/servers in the same project.
        session_start()-> cretes a session or start/continue an existing session
        -->

        <h3>Add Task</h3>

        <form method="POST" action="./server.php">

            <!-- 
                Form method: HOW the data is sent
                Form action: WHERE the data will be sent
             -->

            <input type="hidden" name="action" value="add"/>
            Description: <input type="text" name="description" required/>
            <button type="submit">Add</button>

        </form>

        <h3>Task List</h3>

       <?php if(isset($_SESSION['tasks'])): ?>

        <?php foreach($_SESSION['tasks'] as $index => $task): ?>

        <div>

            <form method="POST" action="./server.php" style="display: inline-block;">

                <input type="hidden" name="action" value="update"/>
                <input type="hidden" name="id" value=" <?= $index; ?>"/>
ygt5fac                <input type="text" name="description" value="<?php echo $task->description; ?>"/>
                <input type="submit" value="Update"/>

            </form>

            <form method="POST" style="display: inline-block;">

                <input type="hidden" name="action" value="remove"/>
                <input type="hidden" name="id" value="<?php $index; ?>"/>
                <input type="submit" value="Delete"/>

            </form>

        </div>

         <?php endforeach; ?>

    <?php endif; ?>

        <br/><br/>

        <form method="POST" action="./server.php">

            <input type="hidden" name="action" value="clear"/>
            <button type="submit">Clear all tasks</button>

        </form>

    </body>

</html>